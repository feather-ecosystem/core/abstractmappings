# AbstractMappings.jl

[![pipeline status](https://gitlab.com/feather-ecosystem/AbstractMappings/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/AbstractMappings/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/AbstractMappings/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/AbstractMappings/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/AbstractMappings/branch/master/graph/badge.svg?token=ESA77DBG7I)](https://codecov.io/gl/feather-ecosystem:core/AbstractMappings)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/AbstractMappings/)

***

This package implements an abstract interface to implement mappings such as geometric mappings, differential forms, tensor fields, etc.

!!! tip
    This package is a part of the [`Feather`](https://gitlab.com/feather-ecosystem) project. It is tightly integrated into the ecosystem of packages provided by Feather. If you are interested in applications of the functionality implemented in this package, please visit the main documentation of the [`ecosystem`](https://feather-ecosystem.gitlab.io/feather/).