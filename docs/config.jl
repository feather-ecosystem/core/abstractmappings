using AbstractMappings

package_info = Dict(
    "modules" => [AbstractMappings],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "AbstractMappings.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/AbstractMappings",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(AbstractMappings, :DocTestSetup, :(using AbstractMappings); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(AbstractMappings, fix=true)
end
