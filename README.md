# AbstractMappings

[![pipeline status](https://gitlab.com/feather-ecosystem/core/abstractmappings/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/core/abstractmappings/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/core/abstractmappings/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/core/abstractmappings/-/commits/master)
<!-- [![codecov](https://codecov.io/gl/feather-ecosystem:core/abstractmappings/branch/master/graph/badge.svg?token=yxL5FDzXwr)](https://codecov.io/gl/feather-ecosystem:core/UnivariateSplines) -->
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/abstractmappings)
